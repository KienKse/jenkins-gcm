package calculoTeste;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import calculo.Fatorial;

public class FatorialMockTest {

	private Integer RESULTADO = 5040;
	private Integer CINCO = 5;
	
	@Mock
	private Fatorial fatorial;

	@Before
	public void init(){
        MockitoAnnotations.initMocks(this); 
   }

	@Test
	public void fatorial5 () {
		Mockito.when(fatorial.calcularFatorial(CINCO)).thenReturn(RESULTADO);
		Integer resultadoEsperado = fatorial.calcularFatorial(CINCO);
		Mockito.verify(fatorial).calcularFatorial(CINCO);
		Assert.assertEquals(resultadoEsperado, RESULTADO);
	}

}