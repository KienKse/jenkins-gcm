package calculoTeste;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import calculo.Fatorial;

public class FatorialTest {
	
	private Fatorial fatorial;
	private int resultado;
	
	@Before
	public void begin () {
		fatorial = new Fatorial();
	}

	@Test
	public void fatorial5 () {
		fatorial.calcularFatorial(5);
		resultado = fatorial.calcularFatorial(5);
		Assert.assertEquals(120, resultado);
	}

	@Test
	public void fatorial7 () {
		fatorial.calcularFatorial(7);
		resultado = fatorial.calcularFatorial(7);
		Assert.assertEquals(5040, resultado);
	}
}