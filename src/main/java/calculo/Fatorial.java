/**
 * 
 */
package calculo;

/**
 * @author Kien
 *
 */
public class Fatorial {

	public int calcularFatorial(int num) {
		if(num == 0){
			return 1;
		}
		return num *= calcularFatorial(num-1);
	}
	
	public static void main(String[] args) {
		Fatorial f1 = new Fatorial();
		System.out.println(f1.calcularFatorial(5));
	}

}
